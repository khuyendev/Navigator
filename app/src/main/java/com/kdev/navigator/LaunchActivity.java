package com.kdev.navigator;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.kdev.navigator.Actionbar.ActionBarLayout;
import com.kdev.navigator.Actionbar.BaseFragment;
import com.kdev.navigator.Components.PasscodeView;
import com.kdev.navigator.Utils.AndroidUtilities;
import com.kdev.navigator.Utils.LayoutHelper;

import java.util.ArrayList;

public class LaunchActivity extends Activity implements ActionBarLayout.ActionBarLayoutDelegate {
    private static ArrayList<BaseFragment> mainFragmentsStack = new ArrayList<>();
    private ActionBarLayout actionBarLayout;
    private PasscodeView passcodeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        NavigatorLoader.init(getApplication());
        super.onCreate(savedInstanceState);
        FrameLayout main = new FrameLayout(LaunchActivity.this);
        main.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
//        setContentView(main);
        actionBarLayout = new ActionBarLayout(this);
        main.addView(actionBarLayout);
        mainFragmentsStack = new ArrayList<>();
        actionBarLayout.init(mainFragmentsStack);
        actionBarLayout.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        actionBarLayout.setDelegate(LaunchActivity.this);
        actionBarLayout.presentFragment(new DemoNewActivity(), false, true, true);
//        passcodeView = new PasscodeView(this);
        setContentView(main, LayoutHelper.createFrame(LayoutHelper.MATCH_PARENT, LayoutHelper.MATCH_PARENT));
//        passcodeView.onShow();
    }

    @Override
    public boolean onPreIme() {
        return false;
    }

    @Override
    public boolean needPresentFragment(BaseFragment fragment, boolean removeLast, boolean forceWithoutAnimation, ActionBarLayout layout) {
        return true;
    }

    @Override
    public boolean needAddFragmentToStack(BaseFragment fragment, ActionBarLayout layout) {
        return true;
    }

    @Override
    public boolean needCloseLastFragment(ActionBarLayout layout) {
        if (layout.fragmentsStack.size() <= 1) {
            finish();
            return false;
        }
        return true;
    }

    @Override
    public void onRebuildAllFragments(ActionBarLayout layout) {

    }

    private void showPasscodeActivity() {
        if (passcodeView == null) {
            return;
        }
        PasscodeView.appLocked = true;
        passcodeView.onShow();
        PasscodeView.isWaitingForPasscodeEnter = true;
        passcodeView.setDelegate(new PasscodeView.PasscodeViewDelegate() {
            @Override
            public void didAcceptedPassword() {
                PasscodeView.isWaitingForPasscodeEnter = false;
                actionBarLayout.showLastFragment();
            }
        });
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        actionBarLayout.onLowMemory();
    }

    @Override
    public void onBackPressed() {
        actionBarLayout.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        actionBarLayout.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        actionBarLayout.onResume();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            actionBarLayout.onKeyUp(keyCode, event);
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        if (mainFragmentsStack != null) {
            mainFragmentsStack.clear();
        }
        mainFragmentsStack = null;
        actionBarLayout = null;
        super.onDestroy();
    }

}
