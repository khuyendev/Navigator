package com.kdev.navigator;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.os.Handler;

import com.kdev.navigator.Components.PasscodeView;

//import com.kdev.navigator.Utils.UserConfig;

/**
 * Created by Khuyen Nguyen on 1/17/2018.
 */

public class NavigatorLoader {
    @SuppressLint("StaticFieldLeak")
    public static volatile Context applicationContext;
    public static volatile Handler applicationHandler;
    public static volatile boolean mainInterfacePaused = false;

    public static void init(Application application) {
        if (applicationContext == null) {
            applicationContext = application;
            applicationHandler = new Handler(application.getMainLooper());
        }
        PasscodeView.loadConfig();
    }

}
