package com.kdev.navigator;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

import com.kdev.navigator.Actionbar.ActionBar;
import com.kdev.navigator.Actionbar.AlertDialog;
import com.kdev.navigator.Actionbar.BaseFragment;
import com.kdev.navigator.Actionbar.BottomSheet;
import com.kdev.navigator.Components.Theme;


/**
 * Created by Khuyen Nguyen on 1/17/2018.
 */

public class DemoNewActivity extends BaseFragment {
    private DemoActivityDelegate delegate;

    public interface DemoActivityDelegate {
        void doSomethings();
    }

    @Override
    public int getLayoutId() {
        return R.layout.dempo;
    }

    @Override
    public void initView() {
        findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presentFragment(new DemoNewActivity(), false, false);
            }
        });
        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DemoNewActivity.this.getParentActivity());
                builder.setTitle("AppName");
                builder.setMessage("hihii");
                builder.setPositiveButton("OK", null);
                Dialog dialog = builder.create();
                DemoNewActivity.this.showDialog(dialog);
            }
        });
        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getParentActivity().onBackPressed();
            }
        });

        findViewById(R.id.button4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheet.Builder builder = new BottomSheet.Builder(DemoNewActivity.this.getParentActivity());
                builder.setTitle("hihiih");
                builder.setItems(new CharSequence[]{"Open", "Copy"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, final int which) {

                    }
                });
                showDialog(builder.create());
            }
        });
        findViewById(R.id.button3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishFragment();
            }
        });
        actionBar.setBackgroundColor(Theme.ACTION_BAR_COLOR);

        actionBar.setItemsBackgroundColor(Theme.ACTION_BAR_SELECTOR_COLOR, false);
        actionBar.setTitle("This is title");
        actionBar.setSubtitle("This is subtitle");
        actionBar.setBackButtonImage(R.drawable.ic_arrow_back_white_24dp);
        actionBar.createMenu().addItem(12, R.drawable.ic_search_white_24dp).setIsSearchField(true).getSearchField().setHint("Search");
        actionBar.createMenu().addItem(12, R.drawable.ic_more_vert_white_24dp).addSubItem(12, "This menu item");

        actionBar.setActionBarMenuOnItemClick(new ActionBar.ActionBarMenuOnItemClick() {
            @Override
            public void onItemClick(int id) {
                if (id == -1) {
                    finishFragment();
                }
            }
        });
    }

    @Override
    public void onFragmentDestroy() {
        super.onFragmentDestroy();
        delegate = null;
    }

    public void setDelegate(DemoActivityDelegate delegate) {
        this.delegate = delegate;
    }
}
