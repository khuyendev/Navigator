
package com.kdev.navigator.Components;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Build;
import android.util.StateSet;

import com.kdev.navigator.Utils.AndroidUtilities;


public class Theme {

    public static final int ACTION_BAR_COLOR = 0xff527da3;
    public static final int ACTION_BAR_TITLE_COLOR = 0xffffffff;
    public static final int ACTION_BAR_MENU_TITLE_COLOR = 0xff212121;
    public static final int ACTION_BAR_SUBTITLE_COLOR = 0xffffffff;
    public static final int ACTION_BAR_DEFAULT_SELECTOR_COLOR = 0xfff0f0f0;
    public static final int ACTION_BAR_DEFAULT_ICON_COLOR = 0xfff0f0f0;
    public static final int ACTION_BAR_MODEL_DEFAULT_ICON_COLOR = 0xff737373;
    public static final int ACTION_BAR_SELECTOR_COLOR = 0xff406d94;
    public static final int ACTION_BAR_SEARCH = 0xffffffff;
    public static final int ACTION_BAR_SEARCH_PLACEHOLDER = 0x88ffffff;


    public static final int key_dialogButtonSelector= 0x0f000000;
    public static final int key_dialogIcon= 0xff8a8a8a;
    public static final int key_dialogTextBlack= 0xff212121;
    public static final int key_dialogBackground= 0xffffffff;
    public static final int key_dialogScrollGlow= 0xfff5f6f7;
    public static final int key_progressCircle= 0xff527da3;
    public static final int key_dialogProgressCircle= 0xff527da3;
    public static final int key_dialogLineProgress= 0xff527da3;
    public static final int key_dialogLineProgressBackground= 0xffdbdbdb;
    public static final int key_dialogTextGray2= 0xff757575;
    public static final int key_dialogButton= 0xff4991cc;
//    public static final int key_windowBackgroundWhiteGrayText6= 0xff757575;
//    public static final int key_windowBackgroundWhiteBlackText= 0xff212121;

    public static final int key_contextProgressInner1= 0xffbfdff6;
    public static final int key_contextProgressOuter1= 0xff2b96e2;
    public static final int key_contextProgressInner2= 0xffbfdff6;
    public static final int key_contextProgressOuter2= 0xffffffff;
    public static final int key_contextProgressInner3= 0xffb3b3b3;
    public static final int key_contextProgressOuter3= 0xffffffff;
//    public static final int key_windowBackgroundWhite= 0xffffffff;
//    public static final int key_actionBarDefaultTitle= 0xffffffff;


    private static Paint maskPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public static Drawable createBarSelectorDrawable(int color) {
        return createBarSelectorDrawable(color, true);
    }

    public static Drawable createBarSelectorDrawable(int color, boolean masked) {
        if (Build.VERSION.SDK_INT >= 21) {
            Drawable maskDrawable = null;
            if (masked) {
                maskPaint.setColor(0xffffffff);
                maskDrawable = new Drawable() {
                    @Override
                    public void draw(Canvas canvas) {
                        android.graphics.Rect bounds = getBounds();
                        canvas.drawCircle(bounds.centerX(), bounds.centerY(),
                                AndroidUtilities.dp(18), maskPaint);
                    }

                    @Override
                    public void setAlpha(int alpha) {

                    }

                    @Override
                    public void setColorFilter(ColorFilter colorFilter) {

                    }

                    @Override
                    public int getOpacity() {
                        return PixelFormat.UNKNOWN;
                    }
                };
            }
            ColorStateList colorStateList = new ColorStateList(
                    new int[][] {
                            new int[] {}
                    },
                    new int[] {
                            color
                    });
            return new RippleDrawable(colorStateList, null, maskDrawable);
        } else {
            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.addState(new int[] {
                    android.R.attr.state_pressed
            }, new ColorDrawable(color));
            stateListDrawable.addState(new int[] {
                    android.R.attr.state_focused
            }, new ColorDrawable(color));
            stateListDrawable.addState(new int[] {
                    android.R.attr.state_selected
            }, new ColorDrawable(color));
            stateListDrawable.addState(new int[] {
                    android.R.attr.state_activated
            }, new ColorDrawable(color));
            stateListDrawable.addState(new int[] {}, new ColorDrawable(0x00000000));
            return stateListDrawable;
        }
    }
    public static Drawable createSelectorDrawable(int color) {
        return createSelectorDrawable(color, 1);
    }
    public static Drawable getRoundRectSelectorDrawable() {
        if (Build.VERSION.SDK_INT >= 21) {
            Drawable maskDrawable = createRoundRectDrawable(AndroidUtilities.dp(3), 0xffffffff);
            ColorStateList colorStateList = new ColorStateList(
                    new int[][]{StateSet.WILD_CARD},
                    new int[]{key_dialogButtonSelector}
            );
            return new RippleDrawable(colorStateList, null, maskDrawable);
        } else {
            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.addState(new int[]{android.R.attr.state_pressed}, createRoundRectDrawable(AndroidUtilities.dp(3), key_dialogButtonSelector));
            stateListDrawable.addState(new int[]{android.R.attr.state_selected}, createRoundRectDrawable(AndroidUtilities.dp(3), key_dialogButtonSelector));
            stateListDrawable.addState(StateSet.WILD_CARD, new ColorDrawable(0x00000000));
            return stateListDrawable;
        }
    }
    public static Drawable createRoundRectDrawable(int rad, int defaultColor) {
        ShapeDrawable defaultDrawable = new ShapeDrawable(new RoundRectShape(new float[]{rad, rad, rad, rad, rad, rad, rad, rad}, null, null));
        defaultDrawable.getPaint().setColor(defaultColor);
        return defaultDrawable;
    }
    public static Drawable getSelectorDrawable(boolean whiteBackground) {
        if (whiteBackground) {
            if (Build.VERSION.SDK_INT >= 21) {
                Drawable maskDrawable = new ColorDrawable(0xffffffff);
                ColorStateList colorStateList = new ColorStateList(
                        new int[][]{StateSet.WILD_CARD},
                        new int[]{0x0f000000}
                );
                return new RippleDrawable(colorStateList, new ColorDrawable(0xffffffff), maskDrawable);
            } else {
                int color = 0x0f000000;
                StateListDrawable stateListDrawable = new StateListDrawable();
                stateListDrawable.addState(new int[]{android.R.attr.state_pressed}, new ColorDrawable(color));
                stateListDrawable.addState(new int[]{android.R.attr.state_selected}, new ColorDrawable(color));
                stateListDrawable.addState(StateSet.WILD_CARD, new ColorDrawable(0xffffffff));
                return stateListDrawable;
            }
        } else {
            return createSelectorDrawable(0x0f000000, 2);
        }
    }
    public static Drawable createSelectorDrawable(int color, int maskType) {
        Drawable drawable;
        if (Build.VERSION.SDK_INT >= 21) {
            Drawable maskDrawable = null;
            if (maskType == 1) {
                maskPaint.setColor(0xffffffff);
                maskDrawable = new Drawable() {
                    @Override
                    public void draw(Canvas canvas) {
                        android.graphics.Rect bounds = getBounds();
                        canvas.drawCircle(bounds.centerX(), bounds.centerY(), AndroidUtilities.dp(18), maskPaint);
                    }

                    @Override
                    public void setAlpha(int alpha) {

                    }

                    @Override
                    public void setColorFilter(ColorFilter colorFilter) {

                    }

                    @Override
                    public int getOpacity() {
                        return PixelFormat.UNKNOWN;
                    }
                };
            } else if (maskType == 2) {
                maskDrawable = new ColorDrawable(0xffffffff);
            }
            ColorStateList colorStateList = new ColorStateList(
                    new int[][]{StateSet.WILD_CARD},
                    new int[]{color}
            );
            return new RippleDrawable(colorStateList, null, maskDrawable);
        } else {
            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.addState(new int[]{android.R.attr.state_pressed}, new ColorDrawable(color));
            stateListDrawable.addState(new int[]{android.R.attr.state_selected}, new ColorDrawable(color));
            stateListDrawable.addState(StateSet.WILD_CARD, new ColorDrawable(0x00000000));
            return stateListDrawable;
        }
    }
}
